-----------------------------------------------

:::::::::::: app checkbirthday  :::::::::::::::

-----------------------------------------------


[Definición:]

Es un app consume el Api Rest de la app birthdayreminder y brinda una visualizacion mas amigable
con el usuarios.

[Detalles de construccion:]

	[Angular CLI](https://github.com/angular/angular-cli) version 9.1.6.
	typeScript  Version 3.9.3
	nodeJs  Version 14.3.0
	npm Version 6.14.5

[Instalacion]

	Situarse dentro del directorio checkbirthday
	Tener instalado las tecnologias antes mencionadas.

	Aplicar una serie de comandos en la CMD DOS de Windows o SHELL de Linux.

	$ npm install

	2 formas de desplegar la app en el entorno local. 

	$ npm run ng build -- -c=production --prod

		1. Con el servidor integrador de angular con los comandos pero solo dentro de la carpeta
		   checkbirthday		

				ng serve -o

			expondra la url : http://localhost:4200/consultar
            
        2. Instalar un servidor web como apache2 o nginx
			copiar la carpeta que se creo dentro del directorio dist a la carpeta del servidor web.
			en el caso de nginx  configurar el archivo nginx/sites-available/default cambiar las directivas

				root /var/www/html/checkbirthday;
				server_name checkbirthda;
