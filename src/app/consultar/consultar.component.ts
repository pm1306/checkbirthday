import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsuarioService } from '../../app/usuario/usuario.service';
import { Usuario } from '../../app/usuario/usuario';

@Component({
  selector: 'app-consultar',
  templateUrl: './consultar.component.html',
  styleUrls: ['./consultar.component.css']
})
export class ConsultarComponent implements OnInit {
  contacto: FormGroup;
  submitted = false;
  usuario: Usuario;
  constructor(private usuarioService: UsuarioService, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.contacto = this.formBuilder.group({
      nombres: ['', Validators.required],
      apellidos: ['', [Validators.required]],
      fecha: ['', Validators.required]
    });
  }

  get f() { return this.contacto.controls; }

  async onSubmit() {
    this.submitted = true;
    if (this.contacto.invalid) {
      return;
    }
    this.consultarUsuarios(this.contacto.get('nombres').value, this.contacto.get('apellidos').value, this.contacto.get('fecha').value);
  }

  async consultarUsuarios(nombres: any, apellidos: any, fecha: any) {
    this.usuarioService.getConsultaCumple(nombres, apellidos, fecha).subscribe(
      usuario => {
        this.usuario = usuario;
        return this.usuario;
      });
  }

}
