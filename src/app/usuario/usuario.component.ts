import { Component, OnInit } from '@angular/core';
import { UsuarioService } from './usuario.service';
import { Usuario } from './usuario';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  public usuariosList: Usuario[];
  constructor(private  usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this.usuarioService.getUsuarios().subscribe(
      usuarios => {
        this.usuariosList=usuarios;
        this.usuariosList.sort((a, b) => (a.id > b.id ? -1 : 1));
      });
      
  }

}
