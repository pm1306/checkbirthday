export class Usuario{
    id:number;
    nombre:string;
    apellido:string;
    fecha:string;
    edad:number;
    poema:string;
    felicitaciones:string;
    faltantes:number;
}