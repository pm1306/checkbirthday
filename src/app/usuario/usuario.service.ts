import { Injectable } from '@angular/core';
import { Usuario } from './usuario';
import { environment } from '../../environments/environment';
import { of, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  usuarios: Usuario[];
  usuario: Usuario;
  urlConParametros: string;
  private url = environment.url_services;
  constructor(private http: HttpClient) { }

  getUsuarios(): Observable<Usuario[]> {
    return this.http.get(this.url + '/userschecked').pipe(
      map((response: any) => {
        this.usuarios = response;
        return this.usuarios;
      }));
  }

  getConsultaCumple(nombres, apellidos, fecha): Observable<Usuario> {
    this.urlConParametros = this.url + '/checkbirthday?nombres=' + nombres + '&apellidos=' + apellidos + '&fecha=' + fecha;
    return this.http.get(this.urlConParametros).pipe(
      map((response: any) => {
        this.usuario = response;
        return this.usuario;
      }));
  }

}
